#include <iostream>
#include <string>
#include <bitset>
#include "Jules_TpSDI.h"

using namespace std;

int main()
{
	/*	Ecrire un programme dont les fonctionnalités sont les suivantes :
	Le programme demande à l’utilisateur de saisir une chaine de caractères du type : +0002.025 (cf.Q12)
		Signe +
		Chiffres avant la virgule 0002
		Chiffres après la virgule 025
		Le programme calcule la valeur en numérique selon les spécifications si dessus
		Le résultat pour l’exemple donné sera : +2.025 mètres
	*/
	//string machaine;
	//cout << "veuillez saisir la reponse au format SDI-12 (+0002.025)" << endl;
	//cin >> machaine;
	//cout << "vous avez saisi : " << machaine << endl;

	//// etape 1 on cherche le signe : il doit se trouver en premier 
	//char signe = machaine[0];
	//if (signe == '+') cout << "chiffre positif" << endl;
	//else if (signe == '-') cout << "chiffre negatif" << endl;
	//else cout << "signe non reconnu" << endl;

	//// etape 2 on cherche les chiffres avant la virgule 
	//int pos = machaine.find('.');

	//// on utlise la conversion 
	//cout << ". trouve a la position " << pos;
	//float val = stof(machaine);
	//cout << " en float = " << val << endl;

	//cout << " =================================" << endl;
	//cout << " =================================" << endl;

	// etape 3
	int tab[10];
	for(int i = 0 ; i < size(tab); i++)
	{
		int val;
		cout << "saisir valeur " << i << endl;
		cin >> hex >> tab[i];
	}

	cout << "\nvaleur binaire : " << endl;
	for (int i = 0; i < size(tab); i++)
	{
		cout << bitset<16>(tab[i]) << "  " ;
	}

	cout << "\nvaleur decimal : " << endl;
	for (int i = 0; i < size(tab); i++)
	{
		cout << dec << tab[i] << "  ";
	}
	// etape 4
}
